from django.contrib.auth.models import AbstractUser
from django.db import models


class Register(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField(max_length=25, unique=True)
    phone = models.IntegerField(default=True, unique=True)
    address = models.CharField(max_length=100, verbose_name='please write currently address')
    nickname = models.CharField(max_length=25)

    class Meta:
        verbose_name = 'Register'
        verbose_name_plural = 'registers'

    def __str__(self):
        return '{}: {}, {}'.format(self.name,
                                   self.email,
                                   self.phone,
                                   self.address,
                                   self.nickname
                                   )


class Category(models.Model):
    brand = models.CharField(max_length=20)
    type = models.SlugField(unique=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.brand


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=99, verbose_name='model of product.')
    additional = models.TextField(
        verbose_name='Please write additional information of your product and understandable.')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Price in $')

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'product'

    def __str__(self):
        return '{}: {}, {}'.format(self.title,
                                   self.additional,
                                   self.price
                                   )


class OrderProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name_product = models.CharField(max_length=50, verbose_name='Writing name your product')
    quantity = models.PositiveIntegerField(default='some_number', blank=True, null=True, verbose_name='Choose Product')
    total_price = models.FloatField(blank=True, null=True, verbose_name='Total price')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        quantity = self.quantity
        price = self.product.price
        self.total_price = int(price * quantity) < int(10000)
        return super(OrderProduct, self).save(force_insert, force_update, using, update_fields)

    arrival_date = models.DateField(null=True, blank=True, verbose_name='Choose arrival day. Min arrival day is 10')

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     if not self.pk:
    #         self.arrival_date = str(timezone.now() + timedelta(days=10))
    #     return super(OrderProduct, self).save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return '{}: {}, {}, {}'.format(self.name_product, self.quantity, self.total_price,
                                       self.arrival_date)
