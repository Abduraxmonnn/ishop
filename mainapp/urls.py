from django.urls import path

from . import views

urlpatterns = [
    path('register/create/', views.RegisterAPIView.as_view(), name='register'),
    path('register/detail/<int:pk>/', views.RegisterDetailAPIView.as_view(), name='registers'),
    path('category/create/', views.CategoryAPIView.as_view(), name='category_create'),
    path('category/detail/<int:pk>/', views.CategoryDetailAPIView.as_view(), name='category'),
    path('product/', views.ProductAPIView.as_view(), name='products'),
    path('product/detail/<int:pk>/', views.ProductDetailAPIView.as_view(), name='product_details'),
    path('order/detail/<int:pk>', views.OrderProductAPIView.as_view(), name='order_product'),
    path('order/choose/', views.OrderProductDetailAPIView.as_view(), name='order_product_detail')
]
