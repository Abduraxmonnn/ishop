# username: admin
# password: admin1234
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import *


class RegisterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'phone', 'address', 'nickname')
    list_filter = ('id', 'name', 'address')
    list_display_links = ('id', )
    search_fields = ('id', 'name', 'email', 'phone', 'address', 'nickname')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'price')
    list_filter = ('id',)
    list_display_links = ('id', )
    search_fields = ('id', 'title', 'price')


class OrderProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name_product', 'quantity', 'total_price', 'arrival_date')
    list_filter = ('id', 'name_product')
    list_display_links = ('id', )
    exclude = ('total_price', 'arrival_date')
    search_fields = ('id', 'name_product', 'quantity', 'total_price', 'arrival_date')


admin.site.register(Register, RegisterAdmin)
admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(OrderProduct, OrderProductAdmin)
