from rest_framework import serializers

from .models import *


class RegisterSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    phone = serializers.IntegerField(required=True)
    address = serializers.CharField(required=True)
    nickname = serializers.CharField(required=True)

    def create(self, validated_data):
        return Register.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('title', instance.name)
        instance.favoriteCity = validated_data.get(
            'address', instance.favoriteCity)
        instance.save()
        return instance()

    class Meta:
        model = Register
        fields = [
            'id',
            'name',
            'email',
            'phone',
            'address',
            'nickname'
        ]


class CategorySerializer(serializers.ModelSerializer):
    brand = serializers.CharField(required=True)
    type = serializers.SlugField(default=True)

    class Meta:
        model = Category
        fields = [
            'id',
            'brand',
            'type'
        ]


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=Category.objects)
    title = serializers.CharField(required=True)
    additional = serializers.CharField(required=True)
    price = serializers.DecimalField(max_digits=10, decimal_places=1, required=True)

    class Meta:
        model = Product
        fields = [
            'title',
            'additional',
            'price',
        ]


class OrderProductSerializers(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects)
    name_product = serializers.CharField(required=True)
    quantity = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)
    total_price = serializers.CharField(required=True)
    arrival_date = serializers.DateField(allow_null=True, required=True)

    class Meta:
        model = OrderProduct
        fields = [
            'product',
            'name_product',
            'quantity',
            'total_price',
            'arrival_date'
        ]
