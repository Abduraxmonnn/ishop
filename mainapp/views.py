from rest_framework.filters import SearchFilter
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView
from .serializers import RegisterSerializer, CategorySerializer, ProductSerializer, OrderProductSerializers
from .models import Register, Category, Product, OrderProduct


# Register --------------------------------
class RegisterAPIView(ListCreateAPIView):
    serializer_class = RegisterSerializer
    queryset = Register.objects.all()
    lookup_field = ['id', 'name', 'nickname']


class RegisterDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RegisterSerializer
    queryset = Register.objects.all()
    lookup_field = 'id'


# Category --------------------------------
class CategoryAPIView(ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'id'


class CategoryDetailAPIView(RetrieveUpdateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'id'


# Product --------------------------------
class ProductAPIView(ListCreateAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    lookup_field = 'id'


class ProductDetailAPIView(RetrieveUpdateAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    lookup_field = 'id'


# Order Product --------------------------------
class OrderProductAPIView(RetrieveUpdateAPIView):
    serializer_class = OrderProductSerializers
    queryset = OrderProduct.objects.all()
    lookup_field = 'id'


class OrderProductDetailAPIView(ListCreateAPIView):
    serializer_class = OrderProductSerializers
    queryset = OrderProduct.objects.all()
    filter_backends = [SearchFilter]
    lookup_field = 'id'
